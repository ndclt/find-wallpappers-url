use crate::wallpaper::Wallpaper;
use anyhow::Result;

use std::io::Read;

/* Commented because only useful during exploration */
/*
fn print_type_of<T>(_: &T) {
    println!("{}", std::any::type_name::<T>());
}
*/

fn deserialize_wallpaper(r: csv::Result<Wallpaper>) -> Result<Wallpaper> {
    let wallpaper: Wallpaper = r?;
    Ok(wallpaper)
}

pub fn create_wallpapper_from_csv(to_read: Box<dyn Read>) -> Result<Vec<Wallpaper>> {
    let mut reader = csv::Reader::from_reader(to_read);
    Ok(reader
        .deserialize()
        .map(deserialize_wallpaper)
        .filter_map(|w| w.ok())
        .collect())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_read_csv() {
        use std::fs::File;
        use std::path::Path;

        let binding_csv_file =
            Path::new(env!("CARGO_MANIFEST_DIR")).join("test_data/wallpaper_to_read.csv");
        let file = Box::new(File::open(binding_csv_file).unwrap());
        let res: Vec<Wallpaper> = create_wallpapper_from_csv(file).unwrap();

        assert_eq!(
            res,
		vec![
		    Wallpaper::new(&String::from("does_not_exist.jpg"), None),
		    Wallpaper::new(
			&String::from("2010_mavericks_competition_edit1.jpg"),
			Some(&String::from("https://upload.wikimedia.org/wikipedia/commons/d/d1/2010_mavericks_competition_edit1.jpg")))
	    ]
        )
    }
}
