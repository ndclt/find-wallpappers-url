use anyhow::Result;
use clap::Parser;
use clap_verbosity_flag::{InfoLevel, Verbosity};
use futures::StreamExt;
use log::{info, warn};
use std::collections::BTreeSet;
use std::fs::File;
use std::io;
use std::io::{Error, ErrorKind, Write};
use std::path::PathBuf;
use std::time::Instant;
use walkdir::{DirEntry, WalkDir};

mod wallpaper;
use wallpaper::Wallpaper;

mod commons_api;
use commons_api::create_wallpaper_from_commons;

mod csv_interface;
use csv_interface::create_wallpapper_from_csv;

/// List of the images of the given folder, find the wikipedia addresses if it exists and write it
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Cli {
    /// The file where to write result (optional, if not given, standard output will be used)
    #[arg(short, long, env)]
    output: Option<String>,
    /// The file where to read existing addresses (optional, if not given, each image will be searched)
    #[arg(short, long, env)]
    input: Option<String>,
    /// The number of call to do in parallel
    #[arg(short, long, default_value_t = 3, env)]
    max_toolserver_calls: usize,
    /// Allows not existing input file
    #[arg(long, default_value_t = false, env)]
    allow_absent_existing_file: bool,
    /// Control the log level
    #[command(flatten)]
    verbose: Verbosity<InfoLevel>,
    /// The folder to explore
    folder_to_explore: String,
}

/* Commented because only useful during exploration */
/*
fn print_type_of<T>(_: &T) {
    println!("{}", std::any::type_name::<T>());
}
*/

fn _is_to_check(entry: DirEntry) -> Result<DirEntry> {
    let current_path = entry.clone().into_path();
    if current_path.is_dir() {
        return Err(Error::new(ErrorKind::Other, "Directory are not to check").into());
    }
    if entry.path_is_symlink() {
        return Err(Error::new(ErrorKind::Other, "Symlink are not to check").into());
    }
    Ok(entry)
}

// crates: error-chain and failure are deprecated, thus I use anyhow

/*
attention la variable de debug est trop large, on se prend tout le html parser en debug
ce qui pique un peu, réduire cela avec la lecture de la doc de env_logger

la page qui n’existe pas redirige vers une page wikipédia qu’il faudra scraper car ce n’est pas
sûr que cela renvoie un status code 404.

Passage par l’api https://magnus-toolserver.toolforge.org/commonsapi.php plutôt que par la page
html qui est galère à parser. Cette api permet de récupérer du xml.

RUST_LOG value: debug,html5ever=off,scraper=off,selectors=off
 */

/*
TODO :
features :

code :
 * mettre en place le cargo update automatique
 * mettre en place la publication de version en tant qu’artifact et faire une release
 * une fois la mise en place de l’update et de la publication de version,
   mettre à jour le job ansible pour aller chercher la nouvelle version sur gitlab


*/

fn _convert_to_filename(entry: PathBuf) -> Result<String> {
    match entry.file_name() {
        None => Err(Error::new(
            ErrorKind::Other,
            "Can't convert entry file name into string",
        )
        .into()),
        Some(str_file_name) => match str_file_name.to_str() {
            None => {
                Err(Error::new(ErrorKind::Other, "can't convert entry file name to string").into())
            }
            Some(new_str_file_name) => Ok(new_str_file_name.into()),
        },
    }
}

/*
suppression d’élément dans une autre liste :
https://stackoverflow.com/questions/64019451/how-do-i-remove-the-elements-of-vector-that-occur-in-another-vector-in-rust
 */

fn remove_from_vec<T>(mut items: Vec<T>, to_remove: Vec<T>) -> Vec<T>
where
    T: std::cmp::Ord,
{
    let to_remove = BTreeSet::from_iter(to_remove);
    items.retain(|e| !to_remove.contains(e));
    items
}

#[tokio::main]
async fn main() -> Result<()> {
    let begin_time = Instant::now();
    let cli = Cli::parse();
    env_logger::Builder::new()
        .filter_level(cli.verbose.log_level_filter())
        .init();
    info!("args: {:?}", cli);
    let client = reqwest::Client::builder()
        .user_agent("commons-client/0.1.0")
        .pool_max_idle_per_host(cli.max_toolserver_calls)
        .build()
        .unwrap();

    let mut existing_wallpapers = match &cli.input {
        Some(to_open) => {
            let open_file = File::open(shellexpand::tilde(to_open).into_owned());
            if cli.allow_absent_existing_file {
                match open_file {
                    Ok(file) => create_wallpapper_from_csv(Box::new(file)).unwrap(),
                    Err(_) => {
                        warn!("file {:?} does not exist", &cli.input);
                        vec![]
                    }
                }
            } else {
                create_wallpapper_from_csv(Box::new(open_file.unwrap())).unwrap()
            }
        }
        None => vec![],
    };

    let existing_files = existing_wallpapers
        .clone()
        .into_iter()
        .map(|w| w.file_name)
        .collect();

    let all_files_in_folder = WalkDir::new(shellexpand::tilde(&cli.folder_to_explore).into_owned())
        .into_iter()
        .filter_map(|e| e.ok())
        .filter_map(|e| _is_to_check(e).ok())
        .map(|e| e.into_path())
        .filter_map(|e| _convert_to_filename(e).ok())
        .collect();

    let files_to_check = remove_from_vec(all_files_in_folder, existing_files);

    /*
    The futures stream iter permet de créer des futures qui seront ensuite appelées par
    rust avec le collect. Le nombre de calls en parallèle est géré par le nombre donné
    en argument dans la fonction buffer_unordered.
    Étonnement, je n’ai pas besoin de tokio::stream (ni réussi à les faire fonctionner).
    utilisation de la réponse ici : https://stackoverflow.com/questions/51044467/how-can-i-perform-parallel-asynchronous-http-get-requests-with-reqwest
     */

    let new_wallpapers_fetched: Vec<Result<Wallpaper>> = futures::stream::iter(
        files_to_check
            .into_iter()
            .map(|file_name| create_wallpaper_from_commons(file_name, &client)),
    )
    .buffer_unordered(cli.max_toolserver_calls)
    .collect()
    .await;

    let new_wallpapers: Vec<Wallpaper> = new_wallpapers_fetched
        .into_iter()
        .filter_map(|e| e.ok())
        .collect();

    if !new_wallpapers.is_empty() {
        existing_wallpapers.extend(new_wallpapers);
        /*
        On ne peut pas créer deux csv::Writer de type différent dans un match.
        Pour éviter cela, on crée donc une destination qui implémente le trait Write
        que l’on passe à la structure csv::Writer
        à tester, si quand on repasse sur un fichier qui existe déjà le Fille::create ne fait pas n’importe quoi
         */
        let destination: Box<dyn Write> = match &cli.output {
            None => Box::new(io::stdout()),
            Some(to_open) => {
                Box::new(File::create(shellexpand::tilde(to_open).into_owned()).unwrap())
            }
        };
        let mut writer = csv::Writer::from_writer(destination);

        for one_el in existing_wallpapers.into_iter() {
            writer.serialize(one_el)?;
        }
    } else {
        info!("no new wallpaper");
    }
    info!("total duration: {:?}", begin_time.elapsed());
    Ok(())
}

#[cfg(test)]
mod tests {}
