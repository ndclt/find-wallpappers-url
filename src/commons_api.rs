use anyhow::anyhow;
use anyhow::Result;
use log::debug;
use quick_xml::events::Event;
use quick_xml::reader::Reader;
use reqwest::Client;
use std::time::Instant;

use crate::wallpaper::Wallpaper;

#[derive(Copy, Clone, Debug)]
enum ParserState {
    Between,
    ReadURL,
}

struct CommonsFileParser {
    state: ParserState,
    url: Option<String>,
}

impl CommonsFileParser {
    pub fn new() -> Self {
        CommonsFileParser {
            state: ParserState::Between,
            url: None,
        }
    }

    pub fn finish(self) -> Option<String> {
        self.url
    }

    pub fn process(&mut self, ev: Event) {
        self.state = match self.state {
            ParserState::Between => match ev {
                Event::Start(e) if e.local_name().into_inner() == b"urls" => ParserState::ReadURL,
                _ => ParserState::Between,
            },
            ParserState::ReadURL => match ev {
                /*
                Lors du test, je travaille avec un fichier bien mis en forme et donc avec plein de
                retour à la ligne. Il faut donc gérer cela avec même si ce ne sera pas le cas du fichier
                xml du web
                */
                Event::Start(e) if e.local_name().into_inner() == b"file" => ParserState::ReadURL,
                Event::Text(e) => {
                    let tmp_string = &e.unescape().unwrap().into_owned();
                    let maybe_url = tmp_string.trim();
                    if !maybe_url.is_empty() {
                        self.url = Some(maybe_url.to_owned());
                        ParserState::Between
                    } else {
                        ParserState::ReadURL
                    }
                }
                Event::End(_) => ParserState::Between,
                _ => ParserState::ReadURL,
            },
        }
    }
}

/* On utilise le reader<&[u8]> car c’est celui ci qu’on va avoir de la requête http et que je n’ai
pas réussi à indiquer que je voulais soit un Reader<BufRead<File>> soit ce dernier. J’ai donc rusé
sur le test pour avoir le bon type.
*/
pub fn parse_xml_file(mut reader: Reader<&[u8]>) -> Result<String> {
    let mut parser = CommonsFileParser::new();
    loop {
        match reader.read_event()? {
            Event::Eof => break,
            ev => parser.process(ev),
        }
    }

    match parser.finish() {
        Some(e) => Ok(e),
        _ => Err(anyhow!("no url found")),
    }
}

pub async fn create_wallpaper_from_commons(
    file_name: String,
    client: &Client,
) -> Result<Wallpaper> {
    let parameters = [
        ("image", file_name.clone()),
        ("languages", String::from("en")),
    ];
    let url = reqwest::Url::parse_with_params(
        "https://magnus-toolserver.toolforge.org/commonsapi.php",
        parameters,
    )
    .ok();
    debug!("url {:?}", url);
    let begin_time = Instant::now();
    let content: String = client.get(url.unwrap()).send().await?.text().await?;
    debug!("get duration: {:?}", begin_time.elapsed());
    let mut reader = Reader::from_str(&content);
    reader.config_mut().trim_text(true);
    match parse_xml_file(reader) {
        Ok(url) => Ok(Wallpaper::new(&file_name, Some(&url))),
        Err(_) => Ok(Wallpaper::new(&file_name, None)),
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_read_xml() {
        use std::fs;
        use std::path::Path;

        let binding_xml_file =
            Path::new(env!("CARGO_MANIFEST_DIR")).join("test_data/commonsapi.php.xml");
        let xml_file = binding_xml_file.to_str().unwrap();
        let xml_content = fs::read_to_string(xml_file).unwrap();
        let reader = Reader::from_str(&xml_content);
        let res = parse_xml_file(reader).unwrap();

        assert_eq!(
            res,
            String::from("https://upload.wikimedia.org/wikipedia/commons/f/f1/Yarra_Panorama.jpg")
        )
    }
}
