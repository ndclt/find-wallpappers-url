use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct Wallpaper {
    pub file_name: String,
    url: Option<String>,
}

impl Wallpaper {
    pub fn new(file_name: &str, url: Option<&str>) -> Self {
        Wallpaper {
            file_name: file_name.to_owned(),
            url: url.map(|url| url.to_owned()),
        }
    }
}
